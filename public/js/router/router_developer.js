







// ROUTER INITIALIZE

routerSetupConfig.initialize = function() {
    console.log('router initialize()');
    this.status.currentPage = this.status.lastPage = this.status.currentRoute = null;


    // Permanent items as react components
    React.render(
        React.createElement( rc.header ),
        document.getElementById('headercontainer')
    ); 
    React.render(
        React.createElement( rc.nav ),
        document.getElementById('navcontainer')
    );    
    React.render(
        React.createElement( rc.loader ),
        document.getElementById('loadercontainer')
    );   


};




// ROUTER ROUTES

routerSetupConfig.routes =  {

    // home page route uses a react component as a page
    '(?*path)': function(f, q){ this.routeTunnel('react', 'home', rc.homePageComponent, f, q) },
    'accordion(/*path)': function(f, q){ this.routeTunnel('react', 'accordion', rc.accordionPageComponent, f, q) },
    '*badroute': function(){ this.navigate('#', {trigger: true}); }
    // for more information on routing try reading http://mrbool.com/backbone-js-router/28001

};






// ROUTER pre

routerSetupConfig.prePageChange =  function(){
    // any code that must happen before every page change ... place here

};





//  Because all the initialize()  functions occur very early before app.status has values like currentPage
//  we need a function to fire once during the start up and after app.status has populated

routerSetupConfig.appStatusNowReady =  function(){



};


