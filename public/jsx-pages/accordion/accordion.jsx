     var Item = React.createClass({
        render: function () {
          return (
              <a href={this.props.dest}>
            	<label>{this.props.label}</label>
                <span>{this.props.type}</span>
              </a>
          	);
        }
      });

      var Items = React.createClass({
        render: function() {
          return (
            <ul>
              {
                this.props.items.map(function(item, i) {
                  return (
                    <li>
                      <Item dest={item.dest} label={item.label} type={item.type} />
                    </li>
                  )
                })
              }
            </ul>
          )
        }
      });

      var Folder = React.createClass({       
        render: function () {
          return (
          	<div>
          		<label>{this.props.label}</label>
                <Items items= {this.props.folder} />
            </div>
          )
        }
      });
      
      var Accordion = React.createClass({        
        render: function () {
          var self = this;            
              return (
                <ul className="itemlist">
                { 
                  self.props.data.map(function(item,i) {
                    return (
                      <li>
                        <Folder label= {item.label} folder={item.folder} />
                      </li>
                      )
                })
              }                
              </ul>
            )
          }
    });

rc.accordionPageComponent = React.createClass({
	render: function () {
	    var tempData = [
	          {
	            label: 'folder title 1',
	            folder: [
	              { 
	                dest: 'http://www.google.com',
	                label: 'this is test 1222',
	                type: 'e-Book'
	              },
	              { 
	                dest: 'http://www.google.com',
	                label: 'this is test 1',
	                type: 'e-Book'
	              }
	            ]
	          },

	          { 
	            label: 'folder title 2',
	            folder: [
	              { 
	                dest: 'http://www.google.com',
	                label: 'this is test 1222',
	                type: 'e-Book'
	              },
		          { 
		            label: 'sub folder title 2',
		            folder: [
		              { 
		                dest: 'http://www.google.com',
		                label: 'this is test 1222',
		                type: 'e-Book'
		              },
		              { 
		                dest: 'http://www.google.com',
		                label: 'this is test 1',
		                type: 'e-Book'
		              }
		            ]
		          }
	            ]
	          }
	     ];       
		return (
			<div id="accordionpage">      	
				<Accordion data={tempData} />
			</div>
		   )
		 }
	});