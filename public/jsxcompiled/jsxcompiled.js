
/**** COMPILED 2015-11-09 ****/

/* public/js/router/rc_header_v1.js */

'use strict';

var rc = {};

/* public/jsx-pages/accordion/accordion.jsx */
var Item = React.createClass({
    displayName: 'Item',

    render: function render() {
        return React.createElement(
            'a',
            { href: this.props.dest },
            React.createElement(
                'label',
                null,
                this.props.label
            ),
            React.createElement(
                'span',
                null,
                this.props.type
            )
        );
    }
});

var Items = React.createClass({
    displayName: 'Items',

    render: function render() {
        return React.createElement(
            'ul',
            null,
            this.props.items.map(function (item, i) {
                return React.createElement(
                    'li',
                    null,
                    React.createElement(Item, { dest: item.dest, label: item.label, type: item.type })
                );
            })
        );
    }
});

var Folder = React.createClass({
    displayName: 'Folder',

    render: function render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'label',
                null,
                this.props.label
            ),
            React.createElement(Items, { items: this.props.folder })
        );
    }
});

var Accordion = React.createClass({
    displayName: 'Accordion',

    render: function render() {
        var self = this;
        return React.createElement(
            'ul',
            { className: 'itemlist' },
            self.props.data.map(function (item, i) {
                return React.createElement(
                    'li',
                    null,
                    React.createElement(Folder, { label: item.label, folder: item.folder })
                );
            })
        );
    }
});

rc.accordionPageComponent = React.createClass({
    displayName: 'accordionPageComponent',

    render: function render() {
        var tempData = [{
            label: 'folder title 1',
            folder: [{
                dest: 'http://www.google.com',
                label: 'this is test 1222',
                type: 'e-Book'
            }, {
                dest: 'http://www.google.com',
                label: 'this is test 1',
                type: 'e-Book'
            }]
        }, {
            label: 'folder title 2',
            folder: [{
                dest: 'http://www.google.com',
                label: 'this is test 1222',
                type: 'e-Book'
            }, {
                label: 'sub folder title 2',
                folder: [{
                    dest: 'http://www.google.com',
                    label: 'this is test 1222',
                    type: 'e-Book'
                }, {
                    dest: 'http://www.google.com',
                    label: 'this is test 1',
                    type: 'e-Book'
                }]
            }]
        }];
        return React.createElement(
            'div',
            { id: 'accordionpage' },
            React.createElement(Accordion, { data: tempData })
        );
    }
});
/* public/jsx-pages/home/home.jsx */
rc.homePageComponent = React.createClass({
    displayName: 'homePageComponent',

    getInitialState: function getInitialState() {
        return _.extend(app.status, {});
    },
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        return React.createElement('div', { id: 'homepage' });
    }
});
/* public/jsx-special/header/header.jsx */
rc.header = React.createClass({
    displayName: 'header',

    render: function render() {
        return React.createElement(
            'h2',
            null,
            'Backbone Multipage Boilerplate'
        );
    }
});

/* public/jsx-special/loader/loader.jsx */

// USAGE

// grandCentral.trigger('loaderStart', 'pageload');
// grandCentral.trigger('loaderStart', 'loadmypanel');
// grandCentral.trigger('loaderEnd', 'pageload');
// grandCentral.trigger('loaderEnd', 'loadmypanel');

// the loader will go away once the stack is emptied

// DEPENDANCY :

// jQuery    $.inArray

rc.loader = React.createClass({
    displayName: 'loader',

    // no need for stack to be bound to data
    // so it is a property of the component and outside of this.state
    stack: [],
    getInitialState: function getInitialState() {
        return {
            show: false
        };
    },
    componentDidMount: function componentDidMount(currentPage) {
        var self = this;
        // unbind before binding in case component unmounts/remounts, optionally use componentWillUnmount   
        grandCentral.off('loaderStart').on('loaderStart', function (uniqueString) {
            if ($.inArray(uniqueString, self.stack) == -1) {
                console.log('loaderStart(' + uniqueString + ')');
                self.stack.push(uniqueString);
                self.setState({ show: true });
            }
        });
        // unbind before binding in case component unmounts/remounts       
        grandCentral.off('loaderEnd').on('loaderEnd', function (uniqueString) {
            var i = $.inArray(uniqueString, self.stack);
            if (i > -1) {
                self.stack.splice(i, 1);
                console.log('loaderEnd(' + uniqueString + ')');
            }
            if (self.stack.length == 0) {
                self.setState({ show: false });
            }
        });
    },
    reset: function reset() {
        this.stack = [];
        this.setState({ show: false });
    },
    render: function render() {
        var classes = this.state.show ? 'active' : '';
        return React.createElement(
            'div',
            { id: 'loader', className: classes },
            React.createElement(
                'div',
                { className: 'loadingmessage' },
                React.createElement('img', { className: 'spinner', src: SiteConfig.assetsDirectory + 'images/ui/spinner.gif' })
            )
        );
    }
});

/* public/jsx-special/nav/nav.jsx */
rc.nav = React.createClass({
    displayName: 'nav',

    getInitialState: function getInitialState() {
        return {
            currentPage: ''
        };
    },
    componentDidMount: function componentDidMount() {
        var self = this;
        // unbind before binding in case component unmounts/remounts, optionally use componentWillUnmount	
        grandCentral.off('pagechange').on('pagechange', function (data) {
            self.setState({
                currentPage: data.currentPage
            });
        });
    },
    getClassNameWithActive: function getClassNameWithActive(arg) {
        var className = 'navitem';
        if (arg == this.state.currentPage) {
            className = className + ' active';
        }
        return className;
    },
    render: function render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'a',
                { className: this.getClassNameWithActive('home'), href: '#' },
                'Home'
            ),
            React.createElement(
                'a',
                { className: this.getClassNameWithActive('accordion'), href: '#/accordion' },
                'Accordion'
            )
        );
    }
});

/* public/jsx-special/parentsadvisory/parentsadvisory.jsx */
rc.parentsadvisory = React.createClass({
    displayName: 'parentsadvisory',

    render: function render() {
        return React.createElement(
            'div',
            { className: 'parentsadvisory' },
            React.createElement(
                'strong',
                null,
                'Don\'t'
            ),
            React.createElement('br', null),
            'let kids',
            React.createElement('br', null),
            'watch this'
        );
    }
});

/* public/jsx-special/quiz/childcomponents/quizitem.jsx */
rc.quizItemComponent = React.createClass({
    displayName: 'quizItemComponent',

    render: function render() {
        return React.createElement(
            'div',
            { className: 'quizitem' },
            React.createElement('input', { type: 'checkbox', key: this.props.key }),
            React.createElement(
                'span',
                null,
                this.props.label
            )
        );
    }
});

/* public/jsx-special/quiz/quiz.jsx */
rc.quizComponent = React.createClass({
    displayName: 'quizComponent',

    render: function render() {
        // loop through and build up an array which we will include in the render
        var theOptions = [];
        _.each(this.props.data.options, function (value, i) {
            theOptions.push(React.createElement(rc.quizItemComponent, { key: i, label: value }));
        });

        return React.createElement(
            'div',
            null,
            React.createElement(
                'div',
                { className: 'quizheader' },
                React.createElement('span', { className: 'leftGraphic' }),
                React.createElement(
                    'span',
                    { className: 'quiztitle' },
                    this.props.data.quiztitle
                ),
                React.createElement('span', { className: 'rightGraphic' })
            ),
            React.createElement(
                'div',
                { className: 'options' },
                theOptions
            ),
            React.createElement(
                'div',
                { className: 'submitquizbtn' },
                'Submit'
            )
        );
    }
});
