
/**** COMPILED 2015-11-09 ****/

/* public/js/router/rc_header_v1.js */

var rc = {};

/* public/jsx-pages/accordion/accordion.jsx */
     var Item = React.createClass({
        render: function () {
          return (
              <a href={this.props.dest}>
            	<label>{this.props.label}</label>
                <span>{this.props.type}</span>
              </a>
          	);
        }
      });

      var Items = React.createClass({
        render: function() {
          return (
            <ul>
              {
                this.props.items.map(function(item, i) {
                  return (
                    <li>
                      <Item dest={item.dest} label={item.label} type={item.type} />
                    </li>
                  )
                })
              }
            </ul>
          )
        }
      });

      var Folder = React.createClass({       
        render: function () {
          return (
          	<div>
          		<label>{this.props.label}</label>
                <Items items= {this.props.folder} />
            </div>
          )
        }
      });
      
      var Accordion = React.createClass({        
        render: function () {
          var self = this;            
              return (
                <ul className="itemlist">
                { 
                  self.props.data.map(function(item,i) {
                    return (
                      <li>
                        <Folder label= {item.label} folder={item.folder} />
                      </li>
                      )
                })
              }                
              </ul>
            )
          }
    });

rc.accordionPageComponent = React.createClass({
	render: function () {
	    var tempData = [
	          {
	            label: 'folder title 1',
	            folder: [
	              { 
	                dest: 'http://www.google.com',
	                label: 'this is test 1222',
	                type: 'e-Book'
	              },
	              { 
	                dest: 'http://www.google.com',
	                label: 'this is test 1',
	                type: 'e-Book'
	              }
	            ]
	          },

	          { 
	            label: 'folder title 2',
	            folder: [
	              { 
	                dest: 'http://www.google.com',
	                label: 'this is test 1222',
	                type: 'e-Book'
	              },
		          { 
		            label: 'sub folder title 2',
		            folder: [
		              { 
		                dest: 'http://www.google.com',
		                label: 'this is test 1222',
		                type: 'e-Book'
		              },
		              { 
		                dest: 'http://www.google.com',
		                label: 'this is test 1',
		                type: 'e-Book'
		              }
		            ]
		          }
	            ]
	          }
	     ];       
		return (
			<div id="accordionpage">      	
				<Accordion data={tempData} />
			</div>
		   )
		 }
	});
/* public/jsx-pages/home/home.jsx */
rc.homePageComponent = React.createClass({
    getInitialState:function(){
        return _.extend(app.status, {
        })
    },    
    render:function(){
        console.log(this.constructor.displayName+' render()');
        return (


<div id="homepage">

</div>




        );
    }
});
/* public/jsx-special/header/header.jsx */
rc.header = React.createClass({
    render:function(){
        return (

<h2>Backbone Multipage Boilerplate</h2>

        );
    }
});

/* public/jsx-special/loader/loader.jsx */

// USAGE

// grandCentral.trigger('loaderStart', 'pageload');
// grandCentral.trigger('loaderStart', 'loadmypanel');
// grandCentral.trigger('loaderEnd', 'pageload');
// grandCentral.trigger('loaderEnd', 'loadmypanel');

// the loader will go away once the stack is emptied


// DEPENDANCY : 

// jQuery    $.inArray


rc.loader = React.createClass({
    // no need for stack to be bound to data 
    // so it is a property of the component and outside of this.state
    stack : [],  
    getInitialState:function(){
        return {
            show : false
        }
    },
    componentDidMount : function(currentPage){
        var self= this;
        // unbind before binding in case component unmounts/remounts, optionally use componentWillUnmount    
        grandCentral.off('loaderStart').on('loaderStart', function(uniqueString){
            if($.inArray(uniqueString, self.stack)==-1){
                console.log('loaderStart(' + uniqueString + ')');
                self.stack.push(uniqueString);
                self.setState({ show: true });
            }

        });
        // unbind before binding in case component unmounts/remounts        
        grandCentral.off('loaderEnd').on('loaderEnd', function(uniqueString){
            var i = $.inArray(uniqueString, self.stack);
            if(i>-1){
                self.stack.splice(i,1);
                console.log('loaderEnd(' + uniqueString + ')');
            }
            if(self.stack.length==0){
                self.setState({ show: false });
            }
        });        
    },    
    reset:function() {
        this.stack = [];
        this.setState({ show: false });
    },
    render:function(){
        var classes = this.state.show ? 'active' : '';
        return (
<div id="loader" className={classes}>
    <div className="loadingmessage">
        <img className="spinner" src={SiteConfig.assetsDirectory+'images/ui/spinner.gif'}/>
    </div>
</div>            
        );
    }
});

/* public/jsx-special/nav/nav.jsx */
rc.nav = React.createClass({
	getInitialState:function(){
        return {
        	currentPage : ''
        }
    },
	componentDidMount : function(){
		var self= this;
        // unbind before binding in case component unmounts/remounts, optionally use componentWillUnmount	
	    grandCentral.off('pagechange').on('pagechange', function(data){
			self.setState({
				currentPage: data.currentPage
			});
	    });
	},
	getClassNameWithActive : function(arg){
		var className = 'navitem';
		if (arg == this.state.currentPage) {
			className = className + ' active';
		}
		return className;
	},
    render:function(){
        return (
<div>
	<a className={this.getClassNameWithActive('home')} href="#">Home</a>
	<a className={this.getClassNameWithActive('accordion')} href="#/accordion">Accordion</a>	
</div>
        );
    }
});
	
/* public/jsx-special/parentsadvisory/parentsadvisory.jsx */
rc.parentsadvisory = React.createClass({
    render:function(){
        return (

<div className="parentsadvisory">
    <strong>Don't</strong>
    <br/>
    let kids
    <br/>
    watch this
</div>

        );
    }
});

/* public/jsx-special/quiz/childcomponents/quizitem.jsx */
rc.quizItemComponent = React.createClass({
    render:function(){
        return (
<div className="quizitem">
	<input type="checkbox" key={this.props.key}/>
    <span>{this.props.label}</span>
</div>
        );
    }
});

/* public/jsx-special/quiz/quiz.jsx */
rc.quizComponent = React.createClass({
    render:function(){
        // loop through and build up an array which we will include in the render
    	var theOptions = [];
    	_.each(this.props.data.options, function(value, i){
    		theOptions.push(<rc.quizItemComponent key={i} label={value}/>);
    	});

        return (
<div>
	<div className="quizheader">
	    <span className="leftGraphic"></span>
	    <span className="quiztitle">{this.props.data.quiztitle}</span>
	    <span className="rightGraphic"></span>
	</div>
	<div className="options">{theOptions}</div>
	<div className="submitquizbtn">Submit</div>
</div>
        );
    }
});
